#!/usr/local/bin/python3.5

import socket

from header import Header
from liquid import Liquid

# Create socket named incoming
incoming = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Incoming listens on 1111
incoming.bind(('', 1111))

incoming.listen(3)

# Start serving.
while True:
    # Unpack incoming connection into tuple.
    (client, addr) = incoming.accept()

    # Instantiate Header object
    header = Header(client.recv(8))
    print("Receiving {} bytes from {}".format(header.size, addr))

    # Grabs the payload one molecule at a time.
    buf = client.recv(header.size - 8)
    while len(buf) < header.size - 8:
        buf += client.recv(header.size - 8 - len(buf))

    # Instantiate Liquid object from the molecules.
    liquid = Liquid(buf)

    liquid.treat_hg()
    liquid.treat_pb()
    liquid.treat_se()


    try:
        header.type = 4
        header.size = 8 + 8*len(liquid.hazmats)
        hazmat_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        hazmat_outgoing.connect(("10.40.13.151", 8888))
        hazmat_outgoing.send(header.serialize())
        hazmat_outgoing.send(liquid.serialize_hazmat())
        hazmat_outgoing.close()
    except:
        print("Could not connect to Hazmat")

    # Pre-treat the liquid.
    liquid.find_trash()
    liquid.find_poo()
    liquid.find_pee()

    try:
    # Create outgoing socket to downstream:2222
        trash_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        trash_outgoing.connect(("10.40.13.151", 2222))
        header.type = 1
        header.size = 8 + 8*len(liquid.trash)
        # Send trash to downstream:2222
        trash_outgoing.send(header.serialize())
        trash_outgoing.send(liquid.serialize_trash())
        trash_outgoing.close()
        print("Sent trash")
    except:
        print("Error with downstream")

    # Create outgoing socket to treatment:4444
    try:
        poo_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        poo_outgoing.connect(("10.40.13.1", 4444))
        header.type = 2
        header.size = 8 + 8*len(liquid.poo)
        # Send poop to treatment:4444
        poo_outgoing.send(header.serialize())
        poo_outgoing.send(liquid.serialize_poo())
        poo_outgoing.close()
        print("Sent poop")

        # Create outgoing socket to treatment:4444
        pee_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        pee_outgoing.connect(("10.40.13.1", 4444))
        header.type = 2
        header.size = 8 + 8*len(liquid.pee)
        # Send pee to treatment:4444
        pee_outgoing.send(header.serialize())
        pee_outgoing.send(liquid.serialize_pee())
        pee_outgoing.close()
        print("Sent pee")

        # Create outgoing socket to treatment:1111
        sewage_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sewage_outgoing.connect(("10.40.13.1", 1111))
        header.type = 0
        header.size = 8 + 8*len(liquid.data)
        # Send remaining liquid to treatment:1111
        sewage_outgoing.send(header.serialize())
        sewage_outgoing.send(liquid.serialize_water())
        sewage_outgoing.close()
        print("Sent Dirty Water")
    except:
        print("Error connecting to treatment")
    # Reset the connection and continue serving.
    client.close()

