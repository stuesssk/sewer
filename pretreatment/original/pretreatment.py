#!/usr/local/bin/python3.5

import socket

from header import Header
from liquid import Liquid

# Create socket named incoming
incoming = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Incoming listens on 1111
incoming.bind(('', 1111))

incoming.listen(3)

# Start serving.
while True:
    # Unpack incoming connection into tuple.
    (client, addr) = incoming.accept()

    # Instantiate Header object
    header = Header(client.recv(8))
    print("Receiving {} bytes from {}".format(header.size, addr))

    # Grabs the payload one molecule at a time.
    buf = client.recv(header.size - 8)
    while len(buf) < header.size - 8:
        buf += client.recv(header.size - 8 - len(buf))

    # Instantiate Liquid object from the molecules.
    liquid = Liquid(buf)

    # Pre-treat the liquid.
    liquid.treat_hg()
    print("Found {} after mercury".format(len(liquid.hazmats)))
    liquid.treat_pb()
    print("Found {} after lead".format(len(liquid.hazmats)))
    liquid.treat_se()
    print("Found {} after selenium".format(len(liquid.hazmats)))

    print("Found {} hazardous contaminants from {}".format(len(liquid.hazmats),
        addr))

    # Create outgoing socket to downstream:8888
    hazmat_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    hazmat_outgoing.connect(("downstream", 8888))
    header.type = 4
    header.size = 8 + 8*len(liquid.hazmats)
    # Send hazmat waste to downstream:8888
    hazmat_outgoing.send(header.serialize())
    hazmat_outgoing.send(liquid.serialize_hazmat())
    hazmat_outgoing.close()

    # Create outgoing socket to treatment:1111
    sewage_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sewage_outgoing.connect(("treatment", 1111))
    header.type = 0
    header.size = 8 + 8*len(liquid.data)
    # Send remaining liquid to treatment:1111
    sewage_outgoing.send(header.serialize())
    sewage_outgoing.send(liquid.serialize_water())
    sewage_outgoing.close()

    # Reset the connection and continue serving.
    client.close()

