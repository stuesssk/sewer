#define _XOPEN_SOURCE 600

#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "networking.h"


const char *short_head = "Did not receive a full header\n";
const char *short_packet = "Did not receive a entire packet\n";
const char *bad_head = "Could not read header\n";
const char *idk = "Unknown Error\n";


int listen_incoming(const char *port)
{
	struct addrinfo *localhost, hints = {0};

	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;


	int errcode = getaddrinfo(NULL, port, &hints, &localhost);
	if(errcode) {
		fprintf(stderr, "Could not get info of localhost:%s: %s",
				port, gai_strerror(errcode));
		return -1;
	}

	int out = socket(localhost->ai_family, localhost->ai_socktype, 0);
	if(out < 0) {
		perror("Could not create socket");
		freeaddrinfo(localhost);
	}

	errcode = bind(out, localhost->ai_addr, localhost->ai_addrlen);
	if(errcode) {
		perror("Unable to bind to localhost");
		close(out);
		freeaddrinfo(localhost);
		return -1;
	}

	freeaddrinfo(localhost);

	errcode = listen(out, 10);
	if(errcode) {
		perror("Unable to listen to localhost");
		close(out);
		return -1;
	}

	return out;
}

int accept_incoming(int sd)
{
	struct sockaddr_storage remote = {0};
	socklen_t sz = sizeof(remote);

	int conn = accept(sd, (struct sockaddr *)&remote, &sz);
	if(conn < 0) {
		perror("Could not accept connection");
	}

	return conn;
}

int connect_outgoing(const char *host, const char *port)
{
	struct addrinfo *downstream, hints = {0};

	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_CANONNAME;

	int errcode = getaddrinfo(host, port, &hints, &downstream);
	if(errcode) {
		fprintf(stderr, "Could not get info of %s:%s: %s",
				host, port, gai_strerror(errcode));
		return -1;
	}

	int out = socket(downstream->ai_family, downstream->ai_socktype, 0);
	if(out < 0) {
		perror("Could not create socket");
		freeaddrinfo(downstream);
	}

	errcode = connect(out, downstream->ai_addr, downstream->ai_addrlen);
	if(errcode) {
		perror("Unable to connect downstream");
		close(out);
		freeaddrinfo(downstream);
		return -1;
	}

	return out;
}


void send_report(int sockd, int err_code, struct header *head)
{
    struct report *report = calloc(1, sizeof(*report));
    if(!report) {
        fprintf(stderr, "Could not get space for report");
        return;
    }

    switch(err_code) {
        case 0:
            report->custom = 0;
            report->error_type = not_enough_data;
            strncpy(report->message, short_head, strlen(short_head));
            break;
        case 1:
            report->custom = 1;
            report->error_type = not_enough_data;
            strncpy(report->message, short_packet, strlen(short_packet));
            break;
        case 2:
            report->custom = 2;
            report->error_type = not_enough_data;
            strncpy(report->message, bad_head, strlen(bad_head));
            break;
        default:
            report->custom = 32;
            report->error_type = not_enough_data;
            strncpy(report->message, idk, strlen(idk));
    }

    report->ip_addr = get_ip(sockd);

    int osludge = connect_outgoing("downstream", "9999");
    if(osludge < 0) {
        perror("Could not connect downstream");
        free(report);
        return;
    }

    write(osludge, head, sizeof(*head));
    write(osludge, report, sizeof(*report));

    close(osludge);
    free(report);

    return;
}


/*
    Source: https://stackoverflow.com/questions/20472072/c-socket-get-ip-address-from-filedescriptor-returned-from-accept
*/
uint32_t get_ip(int sockd)
{
    struct sockaddr_in remote = {0};
	socklen_t sz = sizeof(remote);

    getpeername(sockd, (struct sockaddr *)&remote, &sz);

    uint32_t addr = ntohl(remote.sin_addr.s_addr);

    return addr;
}
