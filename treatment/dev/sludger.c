
#include <libscrypt.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>

#include "networking.h"
#include "structs.h"

int main(void)
{
	int isludge = listen_incoming("4444");
	if(isludge < 0) {
		perror("Could not listen upstream");
		return -1;
	}

	int new_stream = 0;

    signal(SIGCHLD, SIG_IGN);

    // Start serving forever.
	while((new_stream = accept_incoming(isludge))) {

        // Fork when a client connects.
        pid_t child = fork();

        // Start child stuff.
        if(child == 0) {
            fprintf(stderr, "Received connection\n");
            // Close connection to main socket.
            close(isludge);
		    struct header head;

            // Read header, store in head.
		    ssize_t received_bytes = read(new_stream, &head, sizeof(head));
		    if(received_bytes < 0) {
                // Send report, close conn to client, exit.
			    send_report(new_stream, 2, &head);
			    close(new_stream);
			    exit(0);
		    }
		    if((size_t)received_bytes < sizeof(head)) {
                // Send report, close conn to client, exit.
			    send_report(new_stream, 0, &head);
                close(new_stream);
			    exit(0);
		    }

		    // This is the only place where a ntoh_ call is needed
		    size_t items = (ntohs(head.size) - sizeof(head))/8;
            fprintf(stderr, "Got %zd poops\n", items);
		    // Leave the last node as a nice all-0 node
		    struct node *payload = calloc(items+1, sizeof(payload));
		    received_bytes = 0;

		    // Read in the entire payload
		    do {
			    ssize_t amt = read(new_stream, &((char *)payload)[8 + received_bytes], items*8 - received_bytes);
			    if(amt < 0) {
                    // Send report, close conn to client, exit.
                    send_report(new_stream, 1, &head);
                    free(payload);
				    close(new_stream);
                    exit(0);
			    }
			    received_bytes += amt;
		    } while((size_t)received_bytes < items*8);


		    struct hash *hashes = calloc(items, sizeof(*hashes));

            // Hash stuff.
		    for(size_t n = 0; n < items; ++n) {
			    char buf[16];
			    snprintf(buf, sizeof(buf), "%u", ntohl(payload[n+1].data));
                printf("'%s'\n", buf);
			    libscrypt_scrypt((const uint8_t *)buf, strlen(buf),
					    (const uint8_t *)salt, strlen(salt),
					    2048, 4, 4,
					    hashes + n, sizeof(*hashes));
		    }
		    // TODO: This may be too big if there were lots of items
		    // to bake
		    head.size = htons(sizeof(head) + items*sizeof(*hashes));

		    int osludge = connect_outgoing("downstream", "4444");
		    if(osludge < 0) {
                // Send report, close conn to client, exit.
			    perror("Could not connect downstream");
			    close(new_stream);
                exit(0);
		    }

            //head.type = 2;
		    write(osludge, &head, sizeof(head));
		    write(osludge, hashes, items * sizeof(*hashes));

            fprintf(stderr, "Sent stuff\n");
            free(payload);
		    close(osludge);
            exit(0);
        } else if(child < 0) {
            // Fork() went wrong, no child created.
            perror("Could not spawn worker");
            close(new_stream);
        } else {
            // Parent closes connection to client, returns to top of loop.
            close(new_stream);
        }
	}
}
