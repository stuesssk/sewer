
#include <signal.h>
#include <stdio.h>
#include <unistd.h>

#include <arpa/inet.h>

#include "networking.h"
#include "structs.h"

int main(void)
{

	int isludge = listen_incoming("1111");
	if(isludge < 0) {
		perror("Could not listen upstream");
		return -1;
	}

	int new_stream = 0;

    signal(SIGCHLD, SIG_IGN);

    // Start serving forever.
	while((new_stream = accept_incoming(isludge))) {

        // Fork when given a new client.
        pid_t child = fork();

        // Start child stuff.
        if(child == 0) {
            // Close main socket to prevent new incoming connections.
            close(isludge);
            printf("Receiving packets\n");
		    struct header head;

            // Reads packet head, stores in head.
		    ssize_t received_bytes = read(new_stream, &head, sizeof(head));
		    if(received_bytes < 0) {
                // Send report, close conn to client, exit.
			    send_report(new_stream, 2, &head);
			    close(new_stream);
			    exit(0);
		    }
		    if((size_t)received_bytes < sizeof(head)) {
                // Send report, close conn to client, exit.
			    send_report(new_stream, 0, &head);
			    close(new_stream);
			    exit(0);
		    }

		    // This is the only place where a ntoh_ call is needed
		    size_t bytes_to_read = ntohs(head.size) - sizeof(head);
		    // Leave the last node as a nice all-0 node
		    struct node *payload = calloc(bytes_to_read/8 + 1, sizeof(payload));
		    received_bytes = 0;

		    // Read in the entire payload
		    do {
			    ssize_t amt = read(new_stream, &((char *)payload)[8 + received_bytes], bytes_to_read - received_bytes);
			    if(amt < 0) {
                    // Send report, close conn to client, exit.
				    send_report(new_stream, 1, &head);
                    free(payload);
		            close(new_stream);
                    exit(0);
			    }
			    received_bytes += amt;
		    } while((size_t)received_bytes < bytes_to_read);

		    // Phosphate cleaning was handled at the residential plant

		    // Walk the payload items (note the index range), and double-bond 4% of all connections
		    // TODO: This might need to be raised, since there will always be 1 element that has no
		    // child to double-bond to (the tail).

		    int molecules = bytes_to_read/8;
            int cl = molecules * .04;

            if(cl == 0) {
                cl = 1;
            }

	        for(size_t n = 1; n <= cl; n++) {
		        payload[n].left = payload[n].left || payload[n].left;
		        // Chlorination at 4%
	            payload[n].rite = payload[n].left;
	        }

		    // Since nothing about the payload has changed, it can be just sent back out again
		    int owater = connect_outgoing("downstream", "1111");
		    if(owater < 0) {
                // Send report, close conn to client, exit.
			    perror("Could not connect downstream");
			    close(new_stream);
                exit(0);
		    }

            //head.type = 0;
		    write(owater, &head, sizeof(head));
		    // First node is empty just to make offsets easier
		    write(owater, payload + 1, bytes_to_read);

            free(payload);
		    close(owater);
            printf("Sent chlorinated water\n");
            exit(0);
        } else if(child < 0) {
            // Fork() went wrong, no child created.
            perror("Could not spawn worker");
            close(new_stream);
        } else {
            // Parent closes connection to client, returns to top of loop.
            close(new_stream);
        }
	}
}
