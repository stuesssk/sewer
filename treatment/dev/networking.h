
#ifndef NETWORKING_H
 #define NETWORKING_H

#include "structs.h"

int listen_incoming(const char *port);
int accept_incoming(int sd);

int connect_outgoing(const char *host, const char *port);


void send_report(int sockd, int err_code, struct header *head);


uint32_t get_ip(int sockd);

#endif
