#!/usr/bin/python3.5

import socket

from header import Header
from liquid import Liquid

# Create socket named incoming
incoming = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Incoming listens on 1111
incoming.bind(('', 1111))

incoming.listen(3)

# Start serving.
while True:
    # Unpack incoming connection into tuple.
    (client, addr) = incoming.accept()

    # Instantiate Header object
    header = Header(client.recv(8))
    print("Receiving {} bytes from {}".format(header.size, addr))

    # Grabs the payload one molecule at a time.
    buf = client.recv(header.size - 8)
    while len(buf) < header.size - 8:
        buf += client.recv(header.size - 8 - len(buf))

    # Instantiate Liquid object from the molecules.
    liquid = Liquid(buf)

    # Pre-treat the liquid.
    liquid.treat_hg()
    print("Found {} after mercury".format(len(liquid.hazmats)))
    liquid.treat_pb()
    print("Found {} after lead".format(len(liquid.hazmats)))
    liquid.treat_se()
    print("Found {} after selenium".format(len(liquid.hazmats)))

    try:
        # Create outgoing socket to downstream:8888
        header.type = 4
        header.size = 8 + 8*len(liquid.hazmats)
        hazmat_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        hazmat_outgoing.connect(("downstream", 8888))
        # Send hazmat waste to downstream:8888
        hazmat_outgoing.send(header.serialize())
        hazmat_outgoing.send(liquid.serialize_hazmat())
        hazmat_outgoing.close()
        print("Sent Hazmat")
    except:
        print("Hazmat not online")

    # Pre-treat the liquid.
    liquid.find_trash()
    liquid.find_poo()
    liquid.find_pee()

    try:
        # Create outgoing socket to downstream:2222
        header.type = 1
        header.size = 8 + 8*len(liquid.trash)
        trash_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        trash_outgoing.connect(("downstream", 2222))
        # Send trash to downstream:2222
        trash_outgoing.send(header.serialize())
        trash_outgoing.send(liquid.serialize_trash())
        trash_outgoing.close()
        print("Sent trash")
    except:
        print("Trash not online")

    try:
        # Create outgoing socket to treatment:4444
        header.type = 2
        header.size = 8 + 8*len(liquid.poo)
        poo_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        poo_outgoing.connect(("treatment", 4444))
        # Send poop to treatment:4444
        poo_outgoing.send(header.serialize())
        poo_outgoing.send(liquid.serialize_poo())
        poo_outgoing.close()
        print("Sent poop")
    except:
        print("Sludger not online")

    try:
        # Create outgoing socket to treatment:4444
        header.type = 2
        header.size = 8 + 8*len(liquid.pee)
        pee_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        pee_outgoing.connect(("treatment", 4444))
        # Send pee to treatment:4444
        pee_outgoing.send(header.serialize())
        pee_outgoing.send(liquid.serialize_pee())
        pee_outgoing.close()
        print("Sent pee")
    except:
        print("Sludger not online")

    try:
        # Create outgoing socket to treatment:1111
        header.type = 0
        header.size = 8 + 8*len(liquid.data)
        sewage_outgoing = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sewage_outgoing.connect(("treatment", 1111))
        # Send remaining liquid to treatment:1111
        sewage_outgoing.send(header.serialize())
        sewage_outgoing.send(liquid.serialize_water())
        sewage_outgoing.close()
        print("Sent Dirty Water")
    except:
        print("Chlorinator not online")

    # Reset the connection and continue serving.
    client.close()

