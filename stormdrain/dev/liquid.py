#!/usr/local/bin/python3.5

import struct
from math import sqrt

class Node:
    def __init__(self, data, left, right):
        self.data = data
        self.left = left
        self.right = right

class Liquid:
    def __init__(self, blob):
        self.slots = len(blob)//8
        raw_data = struct.unpack("!" + "LHH"*self.slots, blob)
 
        self.hazmats = set()
        self.trash = set()
        self.poo = set()
        self.pee = set()
        self.spore = set()

        self.data = {i//3+1: list(raw_data[i:i+3]) for i in
                range(0, len(raw_data), 3) if raw_data[i]}

    def is_fib(n):
        phi = 0.5 + 0.5 * sqrt(5.0)
        a = phi *n
        return n==0 or abs(round(a)-a) < 1.0 / n


    def treat_spores(self):
        to_remove = set()

        for i in self.data:
            if is_fib(self.data[i][0]):
                self.spore.add(self.data[i][0])
                to_remove.add(i)

        for num in to_remove:
            self.data.pop(num)

        print("Found spore {}\n".format(self.spore))

    def treat_se(self):
        '''Circularly-linked list'''
        heads = set(self.data.keys())
        for k,v in self.data.items():
            heads.discard(v[2])
            heads.discard(v[1])

        if len(heads) < 1:
            to_remove = max(d[0] for d in self.data.values())
            self.hazmats.add(to_remove)

            for i in self.data:
                if self.data[i][0] == to_remove:
                    break
            del self.data[i]
            for n in self.data:
                if self.data[n][1] == i:
                    self.data[n][1] = 0
                if self.data[n][2] == i:
                    self.data[n][2] = 0

    def treat_pb(self):
        pass

    def treat_hg(self):
        '''Multiple roots in graph'''
        heads = set(self.data.keys())
        for k,v in self.data.items():
            heads.discard(v[1])
            heads.discard(v[2])

        if len(heads) > 1:
            to_remove = min((self.data[i][0] for i in heads))
            self.hazmats.add(to_remove)

            for i in self.data:
                if self.data[i][0] == to_remove:
                    break

            del self.data[i]
            self.treat_hg()

    def find_trash(self):
        '''Find trash'''
        to_remove = set()
        air = list()
        print("data = {}\n".format(self.data))
        heads = set(self.data.keys())
        heads.add(0)

        all_trash = False
        for i in self.data:
            if self.data[i][1] not in heads:
                all_trash = True
                self.data[i][1] = 65535
            if self.data[i][2] not in heads:
                all_trash = True
                self.data[i][2] = 65535

        if all_trash == True:
            for i in self.data:
                if self.data[i][0] != 0:
                    self.trash.add(self.data[i][0])
                    to_remove.add(i)
                else:
                    air.append(i)
            for num in to_remove:
                self.data.pop(num)
            for num in air:
                self.data.pop(num)

        print("Trash == {}\n".format(self.trash))

    def find_poo(self):
        '''Find poop'''
        to_remove = set()
        for i in self.data:
            for number in range(2, min(self.data[i][0]//2, 24)):
                if self.data[i][0]%number == 0:
                    self.poo.add(self.data[i][0])
                    to_remove.add(i)
                    break

        for num in to_remove:
            self.data.pop(num)

        print("Found poo {}\n".format(self.poo))

    def find_pee(self):
        '''Find pee'''
        to_remove = set()
        for i in self.data:
            num = str(self.data[i][0])
            num_string = list()
            for digit in num:
                num_string.append(int(digit))
            if len(num_string) == 1 or (len(num_string) < 3 and self.data[i][0]%11 != 0):
                self.pee.add(self.data[i][0])
                to_remove.add(i)
                continue
            length = len(num_string)-2
            greater = False
            und = False
            for x in range(0, length):
                if num_string[x] > num_string[x+1]:
                    if greater == True:
                        und = False
                        break
                    greater = True
                    und = True
                elif num_string[x] < num_string[x+1]:
                    if greater == False:
                        und = False
                        break
                    greater = False
                    und = True
                else:
                    und = False
                    break

            if und == True:
                self.pee.add(self.data[i][0])
                to_remove.add(i)

        for num in to_remove:
            self.data.pop(num)

        print("Found pee {}\n".format(self.pee))

    def serialize_water(self):
        for n in range(self.slots):
            n += 1
            if n not in self.data:
                self.data[n] = (0,0,0)

        array = (x for n in range(self.slots) for x in self.data[n+1])
        return struct.pack("!" + "LHH"*len(self.data), *array)

    def serialize_hazmat(self):
        array = (x for val in self.hazmats for x in (val,0))
        return struct.pack("!" + "LL"*len(self.hazmats), *array)

    def serialize_trash(self):
        array = (x for val in self.trash for x in (val,0))
        return struct.pack("!" + "LL"*len(self.trash), *array)

    def serialize_poo(self):
        array = (x for val in self.poo for x in (val,0))
        return struct.pack("!" + "LL"*len(self.poo), *array)

    def serialize_pee(self):
        array = (x for val in self.pee for x in (val,0))
        return struct.pack("!" + "LL"*len(self.pee), *array)
