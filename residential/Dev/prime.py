#!/usr/local/bin/python3.5

from math import sqrt

number = 2

while(1):
    prime = True
    if number == 2 or number == 3:
        print("prime", number)
        number += 1
        continue
    elif number % 2 == 0 or number % 3 == 0:
        number += 1
        continue
    else:
        for k in range(5,int(sqrt(number)), 2):
            if number % k == 0:
                number += 1
                prime = False
                break
        if prime:
            print("prime", number)
            number += 1
