#!/bin/bash
if ! pgrep -x "chlorinator" > /dev/null
then
    ./chlorinator &
fi

if ! pgrep -x "sludger" > /dev/null
then
    ./sludger &
fi
exit 0
